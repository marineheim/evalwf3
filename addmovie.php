<?php
include_once 'init.php';

// Definition des variables par défaut
$title = null;
$actors = null;
$director = null;
$producer = null;
$yearOfProduct = null;
$language = null;
$category = null;
$storyline = null;
$video = null;

// On controle si l'utilisateur envois le formulaire d'inscription
if (isset($_POST['sendRegistration'])) {

    // Récupération des données du formulaire

    $title         = isset($_POST['title'])           ? $_POST['title']        : null;
    $actors        = isset($_POST['actors'])          ? $_POST['actors']       : null;
    $director      = isset($_POST['director'])        ? $_POST['director']     : null;
    $producer      = isset($_POST['producer'])        ? $_POST['producer']     : null;
    $yearOfProduct = isset($_POST['year_of_product']) ? $_POST['year_of_product'] : null;
    $language      = isset($_POST['language'])        ? $_POST['language'] : null;
    $category      = isset($_POST['category'])        ? $_POST['category'] : null;
    $storyline     = isset($_POST['storyline'])       ? $_POST['storyline'] : null;
    $video         = isset($_POST['video'])           ? $_POST['video'] : null;

    // Peut on enregistrer les données du formulaire dans la BDD ?
    // Par défaut : OUI
    // On surchargera avec la valeur FALSE, dans le cas ou le controle du
    // formulaire détecte une erreur.
    $send = true;




    if ($send) {
        // Enregistrement du film
        $q = "INSERT INTO `movies` (`title`, `actors`, `director`, `producer`, `year_of_product`, `language`, `category`, `storyline`, `video`) VALUES (:movieTitle, :movieActors, :movieDirector, :movieProducer, :movieYearOfProduct, :movieLanguage, : movieCategory, :movieStoryline, :movieVideo)";
        $q = $pdo->prepare($q);
        $q->bindValue(":movieTitle"       , $title        , PDO::PARAM_STR);
        $q->bindValue(":movieActors", $actors , PDO::PARAM_STR);
        $q->bindValue(":movieDirector"   , $director    , PDO::PARAM_STR);
        $q->bindValue(":movieProducer"      , $producer      , PDO::PARAM_STR);
        $q->bindValue(":movieYearOfProduct"      , $yearOfProduct      , PDO::PARAM_STR);
        $q->bindValue(":movieLanguage"      , $language      , PDO::PARAM_STR);
        $q->bindValue(":movieCategory"      , $category      , PDO::PARAM_STR);
        $q->bindValue(":movieStoryline"      , $storyline     , PDO::PARAM_STR);
        $q->bindValue(":movieVideo"      , $video      , PDO::PARAM_STR);
        $q->execute();
        $q->closeCursor();

        // Identification de l'utilisateur

        // Redirection vers sa page de profil
    }

}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Inscription</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>

        <div class="container">


            <?php  ?>


            <h1>Formulaire d'ajout d'un film</h1>
            <form method="post">
                <div>
                    <label for="title">Title</label><br>
                    <input type="text" id="title" name="title" value="<?php echo $title; ?>">
                </div>
                <div>
                    <label for="actors">Acteurs</label><br>
                    <input type="text" id="actors" name="actors" value="<?php echo $actors; ?>">
                </div>
                <div>
                    <label for="director">Directeur</label><br>
                    <input type="text" id="director" name="director" value="<?php echo $director; ?>">
                </div>
                <div>
                    <label for="producer">Producteur</label><br>
                    <input type="text" id="producer" name="producer" value="<?php echo $producer; ?>">
                </div>
                <div>
                    <label for="year_of_product">Producteur</label><br>
                    <select class="" name="year_of_product">

                    </select>
                </div>

                <div>
                    <label for="language">Langue du film</label><br>
                    <select class="" name="language">

                    </select>
                </div>
                <div>
                    <label for="category">Catégories</label><br>
                    <select class="" name="category">

                    </select>
                </div>

                <div>
                    <label for="storyline">Synopsis</label><br>
                    <textarea name="storyline" rows="8" cols="255" value="<?php echo $storyline; ?>"></textarea>
                </div>
                <div>
                    <label for="video">Vidéo</label><br>
                    <input type="text" id="video" name="video" value="<?php echo $video; ?>">
                </div>
                <br>
                <br>
                <button type="submit" name="sendRegistration">Ajouter un film</button>
            </form>
        </body>
    </html>
