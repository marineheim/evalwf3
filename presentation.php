<?php
include_once 'init.php';

// Déclaration du tableau PHP

$users_array = array(
    "Prénom" => "Marine" ,
    "Nom" => "Heim" ,
    "Adresse" => "261, avenue de la République" ,
    "Code postal" => "59110" ,
    "Ville" => "La Madeleine" ,
    "Email" => "marine.heim@outlook.com" ,
    "Téléphone" => "06 63 60 31 70" ,
    "Anniversaire" => "1992-12-31" ,
);


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Présentation</title>
    </head>
    <body>
        <h1>Présentation</h1>

        <!-- On boucle sur le tableau pour afficher le contenu dans une liste -->
        <ul>
        <?php foreach ($users_array as $line => $value): ?>
            <li>
                <?php if( $line == "Anniversaire") {
                    $date = new DateTime($value);
                    // On renvoie le format date au format français
                    echo $line." : ". $date->format('d/m/Y');
                    } else {
                        echo $line." : ". $value;
                } ?>
           </li>
        <!-- On stoppe la boucle -->
        <?php endforeach; ?>

        </ul>

    </body>
</html>
